import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RouterModule } from '@angular/router';

import {MatBadgeModule} from '@angular/material/badge';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSliderModule} from '@angular/material/slider';
import {MatTableModule} from '@angular/material/table';



import { AuditDueComponent } from './dashboard/audit-due/audit-due.component';
import { AuditDoneComponent } from './dashboard/audit-done/audit-done.component';

import {MatInputModule} from '@angular/material/input';
import {FormsModule} from '@angular/forms';

import { MatPaginatorModule } from '@angular/material/paginator';
import {MatCardModule} from '@angular/material/card';
import { AuditPlannedComponent } from './dashboard/audit-planned/audit-planned.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AuditDueComponent,
    AuditDoneComponent,
    AuditPlannedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    RouterModule,
    MatBadgeModule,
    MatButtonModule,
    MatDividerModule,
    MatTabsModule,
    MatSliderModule,
    MatTableModule,
    MatInputModule,
    FormsModule,
    MatPaginatorModule,
    MatCardModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
