import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditPlannedComponent } from './audit-planned.component';

describe('AuditPlannedComponent', () => {
  let component: AuditPlannedComponent;
  let fixture: ComponentFixture<AuditPlannedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuditPlannedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditPlannedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
