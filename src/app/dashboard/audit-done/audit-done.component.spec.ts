import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditDoneComponent } from './audit-done.component';

describe('AuditDoneComponent', () => {
  let component: AuditDoneComponent;
  let fixture: ComponentFixture<AuditDoneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuditDoneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditDoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
