import { Component, OnInit } from '@angular/core';
import {MatTabsModule} from '@angular/material/tabs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  tabNumber:number=1;
  
  constructor() { }

  ngOnInit(): void {
  }
  open(tabNumber:number){
    this.tabNumber=tabNumber;
    console.log(tabNumber);
  }
}
