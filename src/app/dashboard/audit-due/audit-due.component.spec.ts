import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditDueComponent } from './audit-due.component';

describe('AuditDueComponent', () => {
  let component: AuditDueComponent;
  let fixture: ComponentFixture<AuditDueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuditDueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditDueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
