import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { AuditDueComponent } from './dashboard/audit-due/audit-due.component';
import { AuditDoneComponent } from './dashboard/audit-done/audit-done.component';
import { AuditPlannedComponent } from './dashboard/audit-planned/audit-planned.component';





const routes: Routes = [
  { path: 'dashboard/auditDue', component:AuditDueComponent},
  { path: 'dashboard/auditPlanned', component:AuditPlannedComponent},
  { path: 'dashboard/auditDone', component:AuditDoneComponent},
  {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
