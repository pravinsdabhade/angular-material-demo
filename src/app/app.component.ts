import { Component } from '@angular/core';

import { AuditDueComponent } from './dashboard/audit-due/audit-due.component';
import { AuditDoneComponent } from './dashboard/audit-done/audit-done.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-material-demo';
}
